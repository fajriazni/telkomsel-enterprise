		$(document).ready(function () {
			multi_tabs();
			owl_carousel();
			fullnav();
			search_button();
			close_search();
			submit_search();
			submit_search_mobile();
			load_more();
			form_contact();
		});



		$('#MoreBtnAll').on('click', function (e) {
			$('.MoreContentAll').fadeIn();
			$(this).fadeOut();
		});

		$('#MoreBtnAll-1').on('click', function (e) {
			$('.MoreContentAll-1').fadeIn();
			$(this).fadeOut();
		});

		$('#MoreBtnAll-2').on('click', function (e) {
			$('.MoreContentAll-2').fadeIn();
			$(this).fadeOut();
		});

		$('#MoreBtnAll-3').on('click', function (e) {
			$('.MoreContentAll-3').fadeIn();
			$(this).fadeOut();
		});

		$("#close-form").on('click', function (e) {
			$("#contact-us").hide('fast');
		});	
		$(".open-form-contact").on('click', function (e) {
			$("#contact-us").show('fast');
			return false;
		});
		$("#p-next").on('click', function (e) {
			$("#form-step-1").fadeOut();
			$("#page1").fadeOut();
			$("#page2").delay(300).show('fast');
			$('#form-step-2').delay(300).show('fast', function() {
				document.getElementById("input-product").focus();
			});
		});	
		$("#p-prev").on('click', function (e) {
			$("#form-step-2").fadeOut();
			$("#page2").fadeOut();
			$("#page1").delay(300).show('fast');
			$('#form-step-1').delay(300).show('fast', function() {
				document.getElementById("input-nama").focus();
			});
		});	
		$("#btn-next").on('click', function (e) {
			$("#form-step-1").fadeOut();
			$("#page1").fadeOut();
			$("#page2").delay(300).show('fast');
			$('#form-step-2').delay(300).show('fast', function() {
				document.getElementById("input-product").focus();
			});
		});	
		$("#btn-prev").on('click', function (e) {
			$("#form-step-2").fadeOut();
			$("#page2").fadeOut();
			$("#page1").delay(300).show('fast');
			$('#form-step-1').delay(300).show('fast', function() {
				document.getElementById("input-nama").focus();
			});
		});	
		$("#go-to-step-2").on('keypress', function (e) {
			if (e.which == 13) {
				$("#form-step-1").fadeOut();
				$("#page1").fadeOut();
				$("#page2").delay(300).show('fast');
				$('#form-step-2').delay(300).show('fast', function() {
					document.getElementById("input-product").focus();
				});
			}
		});
		$("#input-finnish").on('keypress', function (e) {
			if (e.which == 13) {
				$("#page-number").fadeOut();
				$("#form-step-2").fadeOut();
				$('#form-finnish').delay(300).show('fast', function() {

				});
			}
		});
		$("#submit-contact").on('click', function (e) {
			$("#page-number").fadeOut();
			$("#form-step-2").fadeOut();
			$('#form-finnish').delay(300).show('fast', function() {

			});
		});
		jQuery.extend(jQuery.expr[':'], {
			focusable: function (el, index, selector) {
				return $(el).is('a, button, :input, [tabindex]');
			}
		});
		$(document).on('keypress', 'input,select', function (e) {
			if (e.which == 13) {
				e.preventDefault();
		        // Get all focusable elements on the page
		        var $canfocus = $(':focusable');
		        var index = $canfocus.index(this) + 1;
		        if (index >= $canfocus.length) index = 0;
		        $canfocus.eq(index).focus();
		    }
		});

		function search_button(){
			$("#navbarSearch").click(function(){
				$("#search-nav-full").slideDown();
			});
		}

		function close_search(){
			$("#closeSearch").click(function(){
				$("#search-nav-full").animate({ height: 'toggle', opacity: 'toggle' }, 'slow');
			});
		}

		function submit_search(){
			var input = document.getElementById("mySearchHint");
			input.addEventListener("keyup", function(event) {
				if (event.keyCode === 13) {
					event.preventDefault();
					document.getElementById("SubmitBtn").click();
				}
			});
		}

		function submit_search_mobile(){
			var input = document.getElementById("mySearchHintMobile");
			input.addEventListener("keyup", function(event) {
				if (event.keyCode === 13) {
					event.preventDefault();
					document.getElementById("SubmitBtn").click();
				}
			});
		}

		function owl_carousel(){
			var i;
			for(i=1;i<15;i++){
				$('#owl-' + i).owlCarousel({
					loop:true,
					margin:10,
					responsiveClass:true,
					nav:true,
					autoplay:true,
					autoplayTimeout:4000,
					autoplayHoverPause:true,
					responsive:{
						0:{
							items:1,
							nav:true
						},
						600:{
							items:2,
							nav:false
						},
						1000:{
							items:3,
							nav:false
						},
						1500:{
							items:4,
							nav:false
						}
					}
				})
			}
			$('#owl-testimonial').owlCarousel({
				loop:true,
				margin:10,
				responsiveClass:true,
				nav:true,
				autoplay:true,
				autoplayTimeout:4000,
				autoplayHoverPause:true,
				responsive:{
					0:{
						items:1,
						nav:true
					},
					650:{
						items:2,
						nav:false
					},
					1000:{
						items:3,
						nav:false
					},
					1700:{
						items:4,
						nav:false
					}
				}
			})
			$('#owl-mini-slider').owlCarousel({
				loop:true,
				margin:10,
				responsiveClass:true,
				nav:true,
				autoplay:true,
				autoplayTimeout:4000,
				autoplayHoverPause:true,
				responsive:{
					0:{
						items:2,
						nav:true
					},
					600:{
						items:3,
						nav:false
					},
					1000:{
						items:4,
						nav:false
					},
					1500:{
						items:5,
						nav:false
					}
				}
			})
			$('.main-content #owl-info').owlCarousel({
				loop:true,
				margin:10,
				responsiveClass:true,
				nav:true,
				navText: [
				'<i class="fa fa-caret-left" aria-hidden="true"></i>',
				'<i class="fa fa-caret-right" aria-hidden="true"></i>'
				],
				autoplay:true,
				autoplayTimeout:4000,
				autoplayHoverPause:true,
				navContainer: '.main-content .custom-nav',
				responsive:{
					0:{
						items:1,
						nav:true
					},
					600:{
						items:1,
						av:false
					},
					1000:{
						items:1,
						nav:false
					}
				}
			})
			$('#owl-product').owlCarousel({
				loop:true,
				margin:10,
				responsiveClass:true,
				nav:true,
				autoplay:true,
				autoplayTimeout:4000,
				autoplayHoverPause:true,
				responsive:{
					0:{
						items:1,
						nav:true
					},
					600:{
						items:2,
						av:false
					},
					1000:{
						items:3,
						nav:false
					},
					1200:{
						items:4,
						nav:true,
						nav:false
					}
				}
			})
		}

		function fullnav(){
			$(function() {
				$('#navbarDropdownMenuLink').on('click', function(event) {
					$('#full-sub-nav').slideToggle();
					event.stopPropagation();
				});
				$('#navbarDropdownMenuSearch').on('click', function(event) {
					$('#search-nav').slideToggle();
					event.stopPropagation();
				});
			});
		}

		function multi_tabs(){
			$(document).on('click', '#ueberTab a', function(e) {
				otherTabs = $(this).attr('data-secondary').split(',');
				for(i= 0; i<otherTabs.length;i++) {
					nav = $('<ul class="nav d-none" id="tmpNav"></ul>');
					nav.append('<li class="nav-item"><a href="#" data-toggle="tab" data-target="' + otherTabs[i] + '">nav</a></li>"');
					nav.find('a').tab('show');
				}
				other3Tabs = $(this).attr('data-third').split(',');
				for(i= 0; i<otherTabs.length;i++) {
					nav = $('<ul class="nav d-none" id="tmpNav"></ul>');
					nav.append('<li class="nav-item"><a href="#" data-toggle="tab" data-target="' + other3Tabs[i] + '">nav</a></li>"');
					nav.find('a').tab('show');
				}
			});
		}

		function smooth_load(){
			var i;
			for(i=1;i<20;i++){
				$( "#ct" + i + "-tab").click(function() {
					$('#loadbox').show();
					setTimeout(function () {
						$('#loadbox').hide();
					}, 3000);
				});
			}
			$( "#first-tab").click(function() {
				$('#loadbox').show();
				setTimeout(function () {
					$('#loadbox').hide();
				}, 3000);
			});
			$( "#second-tab").click(function() {
				$('#loadbox').show();
				setTimeout(function () {
					$('#loadbox').hide();
				}, 3000);
			});
			$( "#third-tab").click(function() {
				$('#loadbox').show();
				setTimeout(function () {
					$('#loadbox').hide();
				}, 3000);
			});
		}

		function load_more(){
			$('#MoreBtn').click(function(e){
				$(this).closest('.row').find('.MoreContent:not(:visible):lt(4)').fadeIn();
			});
		}

		/* ===== Dummy Auto Search / Sugestion ===== */
		function autocomplete(inp, arr) {
		  /*the autocomplete function takes two arguments,
		  the text field element and an array of possible autocompleted values:*/
		  var currentFocus;
		  /*execute a function when someone writes in the text field:*/
		  inp.addEventListener("input", function(e) {
		  	var a, b, i, val = this.value;
		  	/*close any already open lists of autocompleted values*/
		  	closeAllLists();
		  	if (!val) { return false;}
		  	currentFocus = -1;
		  	/*create a DIV element that will contain the items (values):*/
		  	a = document.createElement("DIV");
		  	a.setAttribute("id", this.id + "autocomplete-list");
		  	a.setAttribute("class", "autocomplete-items");
		  	/*append the DIV element as a child of the autocomplete container:*/
		  	this.parentNode.appendChild(a);
		  	/*for each item in the array...*/
		  	for (i = 0; i < arr.length; i++) {
		  		/*check if the item starts with the same letters as the text field value:*/
		  		if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
		  			/*create a DIV element for each matching element:*/
		  			b = document.createElement("DIV");
		  			/*make the matching letters bold:*/
		  			b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
		  			b.innerHTML += arr[i].substr(val.length);
		  			/*insert a input field that will hold the current array item's value:*/
		  			b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
		  			/*execute a function when someone clicks on the item value (DIV element):*/
		  			b.addEventListener("click", function(e) {
		  				/*insert the value for the autocomplete text field:*/
		  				inp.value = this.getElementsByTagName("input")[0].value;
		              /*close the list of autocompleted values,
		              (or any other open lists of autocompleted values:*/
		              closeAllLists();
		          });
		  			a.appendChild(b);
		  		}
		  	}
		  });
		  /*execute a function presses a key on the keyboard:*/
		  inp.addEventListener("keydown", function(e) {
		  	var x = document.getElementById(this.id + "autocomplete-list");
		  	if (x) x = x.getElementsByTagName("div");
		  	if (e.keyCode == 40) {
		        /*If the arrow DOWN key is pressed,
		        increase the currentFocus variable:*/
		        currentFocus++;
		        /*and and make the current item more visible:*/
		        addActive(x);
		      } else if (e.keyCode == 38) { //up
		        /*If the arrow UP key is pressed,
		        decrease the currentFocus variable:*/
		        currentFocus--;
		        /*and and make the current item more visible:*/
		        addActive(x);
		    } else if (e.keyCode == 13) {
		    	/*If the ENTER key is pressed, prevent the form from being submitted,*/
		    	e.preventDefault();
		    	if (currentFocus > -1) {
		    		/*and simulate a click on the "active" item:*/
		    		if (x) x[currentFocus].click();
		    	}
		    }
		});
		  function addActive(x) {
		  	/*a function to classify an item as "active":*/
		  	if (!x) return false;
		  	/*start by removing the "active" class on all items:*/
		  	removeActive(x);
		  	if (currentFocus >= x.length) currentFocus = 0;
		  	if (currentFocus < 0) currentFocus = (x.length - 1);
		  	/*add class "autocomplete-active":*/
		  	x[currentFocus].classList.add("autocomplete-active");
		  }
		  function removeActive(x) {
		  	/*a function to remove the "active" class from all autocomplete items:*/
		  	for (var i = 0; i < x.length; i++) {
		  		x[i].classList.remove("autocomplete-active");
		  	}
		  }
		  function closeAllLists(elmnt) {
		    /*close all autocomplete lists in the document,
		    except the one passed as an argument:*/
		    var x = document.getElementsByClassName("autocomplete-items");
		    for (var i = 0; i < x.length; i++) {
		    	if (elmnt != x[i] && elmnt != inp) {
		    		x[i].parentNode.removeChild(x[i]);
		    	}
		    }
		}
		/*execute a function when someone clicks in the document:*/
		document.addEventListener("click", function (e) {
			closeAllLists(e.target);
		});
	}

	/*An array containing all the country names in the world:*/
	var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

	/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
	autocomplete(document.getElementById("mySearchHint"), countries);
	autocomplete(document.getElementById("mySearchHintMobile"), countries);
		/* ===== End Dummy Auto Search / Sugestion ===== */